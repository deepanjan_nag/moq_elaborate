﻿using Moq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoqDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Mock<IBand> mock = new Mock<IBand>();
            //mocks are assessed from top to bottom...and the LAST one to satisfy a condition is applied.
            mock.Setup(m => m.Parser(It.IsAny<int>())).Returns<int>(x => 2*x);
            mock.Setup(m => m.Parser(It.Is<int>(v => v > 10))).Returns<int>(x => x * 5);
            mock.Setup(m => m.Parser(It.Is<int>(v => v > 100))).Returns<int>(x => x * 10);
            mock.Setup(m => m.Parser(It.IsInRange<int>(50, 80, Range.Inclusive))).Returns<int>(x => x+1);
            mock.Setup(m => m.Parser(It.Is<int>(v => v < 0))).Throws<ArgumentOutOfRangeException>();


            var kernel = new StandardKernel();
            kernel.Bind<IBand>().ToConstant(mock.Object);

            var realBand = kernel.Get<IBand>();
            Console.WriteLine(realBand.Parser(1));
            Console.WriteLine(realBand.Parser(11));
            Console.WriteLine(realBand.Parser(101));
            Console.WriteLine(realBand.Parser(60));
            Console.WriteLine(realBand.Parser(-1));
            Console.ReadLine();

        }
    }
    public interface IBand
    {
        int Parser(int i);
    }
}
